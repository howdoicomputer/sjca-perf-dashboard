from django.contrib import admin
from .models import RSOpExpenditure, RSAuthPosition

admin.site.register(RSOpExpenditure)
admin.site.register(RSAuthPosition)
